package scrape

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/tebeka/selenium"
)

const (
	ScreenWidth  = 1280
	ScreenHeight = 966
)

var (
	lastscreen    []byte
	ScreenOffsetY = 1
)

//ScrapeTarget information
//Initialise the scraper
type ScrapeTarget struct {
	Name    string   `json:"name"`
	Domain  string   `json:"domain"`
	PageURL string   `json:"pageurl"`
	PostURL string   `json:"posturl"`
	Pages   []string `json:"pages"`
	Items   map[string]*NewsItem
}

//NewsItem post information
//Exported Information
type NewsItem struct {
	ID       string     `json:"id"`
	Title    string     `json:"title"`
	URL      string     `json:"url"`
	Post     string     `json:"post"`
	Topic    string     `json:"topic"`
	Comments []*Comment `json:"comments"`
}

//Comment item
//Exported Comments for NewsItem.Post
type Comment struct {
	Name    string `json:"name"`
	Comment string `json:"comment"`
	Age     string `json:"age"`
}

type ScrapeDefinition struct {
	Target         *ScrapeTarget    `json:"target"`
	Entry          *SeleniumElement `json:"entry"`
	Comment        *SeleniumElement `json:"comment"`
	EntryDef       []*ScrapElements `json:"ed"`
	CommentDef     []*ScrapElements `json:"cd"`
	ScrapePosts    func(wd selenium.WebDriver, root selenium.WebElement, eles []*ScrapElements) (*NewsItem, error)
	ScrapeComments func(wd selenium.WebDriver, page *NewsItem, entry *SeleniumElement, eles []*ScrapElements) ([]*Comment, error)
}

//SeleniumElement definition
//by. Type of element
type SeleniumElement struct {
	By   string `json:"by"`
	Name string `json:"name"`
}

//ScrapElements to be scrapped from endpoint
type ScrapElements struct {
	Root         bool
	ID           string
	Required     bool
	Find         SeleniumElement `json:"find"`
	GetAttribute string
	Text         bool
	Prefix       string
	Result       string `json:"-"`
}

type ScrapeInterface interface {
	Process(wd selenium.WebDriver, definition *ScrapeDefinition) error
}

func (def *ScrapeDefinition) Process(wd selenium.WebDriver, name string) error {
	return ProcessCrawl(wd, def, name)
}

//Process Crawl
func ProcessCrawl(wd selenium.WebDriver, definition *ScrapeDefinition, name string) error {

	data := definition.Target

	for range data.Pages {

		name := data.Name
		purl := data.Domain + data.PageURL

		LoadPage(wd, purl, name)

		//find all posts
		elems, err := wd.FindElements(
			definition.Entry.By,
			definition.Entry.Name,
		)
		if err != nil {
			return err
		}

		//loop through posts and extract comments
		for _, ele := range elems {

			//take screenshot if image hasn't already been taken (scroll position, compare bytes of lastScreenshot)
			//TakeElementScreenshot(wd, ele, root+name+strconv.Itoa(ScreenOffsetY))
			TakeElementScreenshot(wd, ele, name)

			//Newsitem, err := definition.ScrapePosts(ele, data.domain, data.postURL)
			Newsitem, err := definition.ScrapePosts(wd, ele, definition.EntryDef)

			if err != nil {
				log.Println(err)
				continue
			}

			//strip out non relavent comments/posts
			//match post
			/*
				m := regexp.MustCompile("[gG]ame[ s]")
				r := m.FindStringSubmatch(Newsitem.Title)
				if len(r) == 0 {
					//skip if not found.
					//continue
				} else {
					fmt.Printf("found MATCH{%s}", "[gG]ame[s]")
				}
			*/
			data.Items[Newsitem.ID] = Newsitem
		}

	}

	//loop back over posts and collect comments

	for id, page := range data.Items {
		comments, err := definition.ScrapeComments(wd, page, definition.Entry, definition.CommentDef)
		if err != nil {
			fmt.Println(err)
		}
		data.Items[id].Comments = comments
	}

	return nil

}

//TakeElementScreenshot take screen shot if element in view else scroll and check again
func TakeElementScreenshot(wd selenium.WebDriver, ele selenium.WebElement, name string) {

	for {
		//isdisplayed, err := ele.IsDisplayed()
		view, _ := ele.LocationInView()

		//already in view no need to scroll
		if view.Y < (ScreenHeight * ScreenOffsetY) {
			break
		} else {
			//update scroll for this screen.
			ScreenOffsetY++
			o := strconv.Itoa(ScreenHeight * ScreenOffsetY)
			script := "window.scrollTo(0, " + o + ");"
			_, err := wd.ExecuteScriptRaw(script, nil)
			if err != nil {
				log.Println("unable to scroll", err)
			}
			time.Sleep(time.Millisecond * 100)
		}
	}

	//screenshots repeat at bottom of page.
	//need to check last offset to see if image has already been taken
	ibytes, err := wd.Screenshot()
	if err != nil {
		log.Println(err)
	}

	if bytes.Compare(ibytes, lastscreen) == 0 {
		//same ignore
		return
	}

	//write screenshot
	lastscreen = TakeScreenshot(wd, name+strconv.Itoa(ScreenOffsetY))
}

//ParseHumanTime convert to formatted date string
func ParseHumanTime(s, layout string) (string, error) {

	var (
		multiplier int64 = 1
		p                = "[0-9]+"
		t                = time.Now()
	)

	//always work in mins.
	if strings.Contains(s, "hour") {
		multiplier = 60
	}
	if strings.Contains(s, "days") {
		multiplier = 60 * 24
	}

	m := regexp.MustCompile(p)
	r := m.FindStringSubmatch(s)

	if len(r) == 0 {
		return s, errors.New("unable to parse time")
	}

	if r[0] == "" {
		return s, errors.New("unable to parse time")
	}

	itime, err := strconv.ParseInt(r[0], 10, 64)
	if err != nil {
		return s, err
	}

	mins := itime * multiplier
	dur := time.Duration(-mins * int64(time.Minute))
	then := t.Add(dur)
	return then.Format(layout), nil
}

//TakeScreenshot of current view
func TakeScreenshot(wd selenium.WebDriver, name string) []byte {
	sb, _ := wd.Screenshot()
	sfile, _ := os.Create(name + ".png")
	defer sfile.Close()
	sfile.Write(sb)
	return sb
}

func ExportDefinition(filename string, definition *ScrapeDefinition) error {
	fileEx, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer fileEx.Close()

	encf := json.NewEncoder(fileEx)
	encf.SetIndent("", "    ")
	if err := encf.Encode(definition); err != nil {
		return err
	}
	return nil
}
