package scrape

import (
	"errors"
	"fmt"
	"log"
	"runtime"
	"strings"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/firefox"
)

//Selenium standalone server
const seleniumPath = "dep/selenium-server-standalone.jar"

var (
	port = 8080
	// These paths will be different depending on system
	// Defaults to linux [firefox]
	geckoDriverPath = "dep/firefox/geckodriver-linux"
)

//LoadPage wd
func LoadPage(wd selenium.WebDriver, purl, name string) error {
	if err := wd.Get(purl); err != nil {
		return err
	}
	log.Println("LOAD PAGE:", purl)
	return nil
}

//SeleniumService return new instance of selenium.Service
func SeleniumService() (*selenium.Service, error) {
	switch runtime.GOOS {
	case "windows":
		geckoDriverPath = "dep/firefox/geckodriver.exe"
	case "darwin":
		geckoDriverPath = "dep/firefox/geckodriver-mac"
	}
	opts := []selenium.ServiceOption{
		selenium.GeckoDriver(geckoDriverPath), // Specify the path to GeckoDriver in order to use Firefox.
	}
	//selenium.SetDebug(true)
	service, err := selenium.NewSeleniumService(seleniumPath, port, opts...)
	if err != nil {
		return service, err
	}
	return service, nil
}

//Connect to driver
func Connect() (selenium.WebDriver, error) {

	const path = "./profile"
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}
	f := firefox.Capabilities{}
	if err := f.SetProfile(path); err != nil {
		log.Fatalf("f.SetProfile(%q) returned error: %v", path, err)
	}

	caps.AddFirefox(f)

	wd, err := selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", port))
	if err != nil {
		return nil, err
	}

	return wd, nil
}

func WritePost(n *NewsItem, ele []*ScrapElements) {
	for _, el := range ele {
		switch el.ID {
		case "ID":
			n.ID = el.Result
			break
		case "Title":
			n.Title = el.Result
			break
		case "Post":
			n.Post = el.Result
			break
		case "URL":
			n.URL = el.Result
			break
		}
	}
}

func WriteComment(c *Comment, ele []*ScrapElements) {
	for _, el := range ele {
		switch el.ID {
		case "Name":
			c.Name = el.Result
			break
		case "Comment":
			c.Comment = el.Result
			break
		case "Age":
			c.Age = el.Result
			break
		}
	}
}

func ResolveScrapeElements(wd selenium.WebDriver, root selenium.WebElement, eles []*ScrapElements) (*NewsItem, error) {
	var err error
	item := &NewsItem{
		ID:       "",
		Title:    "",
		Post:     "",
		URL:      "",
		Comments: make([]*Comment, 0),
	}

	for _, el := range eles {
		log.Println("NEW ITEM...", el.ID)
		var declared selenium.WebElement

		var content string
		if el.Root == true {
			declared = root
		} else {
			log.Println("LOOK UP", el.Find.By, el.Find.Name)
			declared, err = root.FindElement(el.Find.By, el.Find.Name)
			if err != nil {
				log.Println(err)
				if el.Required {
					continue
				}
			}
		}

		class, _ := declared.GetAttribute("class")

		if strings.Contains(class, "promoted") {
			log.Println("CLASS INFO:", class)
			return item, errors.New("exclude promoted articles")
		}

		if el.Text == true {
			content, err := declared.Text()
			if err != nil {
				log.Println(err)
				if el.Required {
					continue
				}
			}
			el.Result = el.Prefix + content
			log.Println("using text value of element", el.ID, ":", el.Prefix+content)
			continue
		}

		content, err = declared.GetAttribute(el.GetAttribute)
		if err != nil {
			log.Println(err)
			if el.Required {
				continue
			}
		}
		el.Result = el.Prefix + content
		log.Println("using attr value of element", el.ID, ":", el.Prefix+content)
	}

	WritePost(item, eles)
	return item, err
}

func PAggrigator(wd selenium.WebDriver, root selenium.WebElement, eles []*ScrapElements) (*NewsItem, error) {
	return ResolveScrapeElements(wd, root, eles)
}

func CAggrigator(wd selenium.WebDriver, page *NewsItem, entry *SeleniumElement, eles []*ScrapElements) ([]*Comment, error) {

	comments := make([]*Comment, 0)

	//load the url
	if err := wd.Get(page.Post); err != nil {
		fmt.Println("error getting comments page", err)
		return comments, err
	}

	//collect comment entries
	entries, err := wd.FindElements(entry.By, entry.Name)
	if err != nil {
		fmt.Println("error finding comments", err)
		return comments, err
	}

	//for each comment entry
	for _, ent := range entries {
		comment := &Comment{}
		ResolveScrapeElements(wd, ent, eles)
		WriteComment(comment, eles)
		if comment.Comment != "" {
			comments = append(comments, comment)
		}
	}
	return comments, nil
}
